// STL
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <map>
#include <string>
#include <typeinfo>
#include <vector>

// HAP
#include "sash/DataSet.hh"
#include "sash/EventHeader.hh"
#include "sash/EventSelection.hh"
#include "sash/HESSArray.hh"
#include "sash/IntensityData.hh"
#include "sash/PointerSet.hh"
#include "sash/PointerSetIterator.hh"
#include "sash/RunHeader.hh"
#include "sash/Telescope.hh"
#include "sash/TelescopeConfig.hh"
#include "sash/Pixel.hh"
#include "sash/PixelConfig.hh"
#include "montecarlo/MCTrueShower.hh"
#include "muon/MuonParameters.hh"
// #include "reco/HillasParameters.hh"
// #include "reco/Shower.hh"
#include "stash/Coordinate.hh"

// ROOT
#include <TFile.h>

// HDF5
#include "H5Cpp.h"

typedef struct {
  // Header
  unsigned int eventID;
  int tels_in_event[5] = {};
  std::string event_tels_str;
  int num_of_images;
  // images_data
  std::vector<float> tel1_image, tel2_image, tel3_image, tel4_image, tel5_image;
  // std::vector<std::string> tel1_pixel_ID, tel2_pixel_ID, tel3_pixel_ID, tel4_pixel_ID, tel5_pixel_ID;
  std::vector<int> tel1_pixel_ID, tel2_pixel_ID, tel3_pixel_ID, tel4_pixel_ID, tel5_pixel_ID;
  hvl_t tel1_image_Handle, tel2_image_Handle, tel3_image_Handle, tel4_image_Handle, tel5_image_Handle;
  hvl_t tel1_pixel_ID_Handle, tel2_pixel_ID_Handle, tel3_pixel_ID_Handle, tel4_pixel_ID_Handle, tel5_pixel_ID_Handle;
  // MC_data
  unsigned long mcID		= -1;
  unsigned long showerID     	= -1;
  int primaryID            	= -1;
  float energy               	= -1.0;
  float alt                  	= -1.0;
  float az                   	= -1.0;
  float coreX                	= -1.0;
  float coreY                	= -1.0;
  float coreZ                	= -1.0;
  float xStart               	= -1.0;
  float xMax                 	= -1.0;
  float firstInteractionAlt  	= -1.0;

} event_data_struct;

typedef struct {
  unsigned int run_number;
  std::string tels_str;
  int ntels        = -1;
  float obspos_alt = -1.0;
  float obspos_az  = -1.0;
} run_data_struct;

template <class T>
int numDigits(T number)
{
    int digits = 0;
    if (number < 0) digits = 1; // remove this line if '-' counts as a digit
    while (number) {
        number /= 10;
        digits++;
    }
    return digits;
}

void fill_mc_data( event_data_struct &event_data, Sash::HESSArray* hess ){
  const Sash::MCTrueShower* mcshower = hess->Get<Sash::MCTrueShower>();
  if (mcshower != NULL){
    event_data.mcID                = mcshower->GetGenEventNum();
    event_data.showerID            = mcshower->GetShowerNum();
    event_data.primaryID           = mcshower->GetPrimaryID();
    event_data.energy              = mcshower->GetEnergy();

    Stash::Coordinate mcaltaz 	   = mcshower->GetDirection().GetCoordinate(*hess->GetAltAzSystem());
    event_data.az                  = mcaltaz.GetLambda().GetDegrees();
    event_data.alt                 = mcaltaz.GetBeta().GetDegrees();

    event_data.coreX               = mcshower->GetCore().GetCoordinate(*hess->GetTiltedSystem()).GetX();
    event_data.coreY               = mcshower->GetCore().GetCoordinate(*hess->GetTiltedSystem()).GetY();
    event_data.coreZ               = mcshower->GetCore().GetCoordinate(*hess->GetTiltedSystem()).GetZ();

    event_data.firstInteractionAlt = mcshower->GetIniHeight();
    event_data.xStart              = mcshower->GetDepthStart();
    event_data.xMax                = mcshower->GetShowerMax();
  }
}// end fill_mc_data

void write_MCIntensityDataevent_to_event_struct( event_data_struct &event_data, Sash::HESSArray* hess, int a )
{
  fill_mc_data( event_data, hess );

  const Sash::EventHeader* event_header = hess->Get<Sash::EventHeader>();

  event_data.eventID  =  event_header->GetGlobalEvtNum();

  if( !event_header ) {
    std::cout << "Error: cannot access event header for event # " << a << std::endl;
    return;
  }

  const Sash::EventSelection* event_sel = hess->Get<Sash::EventSelection>();
  const Sash::PointerSet<Sash::Telescope> &tels_with_data = (event_sel) ? event_sel->GetTelSelected() : event_header->GetTelWData();

  for( Sash::PointerSet<Sash::Telescope>::const_iterator tel_it = tels_with_data.begin(); tel_it != tels_with_data.end(); ++tel_it ) {

    std::vector<float>* pixel_intensity 	= NULL;
    std::vector<int>* pixel_ID		= NULL;

    int telescope_number = (*tel_it)->GetId();

    switch( telescope_number ){
    case 1 : pixel_intensity 				= &(event_data.tel1_image);
      pixel_ID 						= &(event_data.tel1_pixel_ID);
      event_data.tels_in_event[0] 	= 1;
      break;
    case 2 : pixel_intensity 				= &(event_data.tel2_image);
      pixel_ID 						= &(event_data.tel2_pixel_ID);
      event_data.tels_in_event[1] 	= 2;
      break;
    case 3 : pixel_intensity 				= &(event_data.tel3_image);
      pixel_ID 						= &(event_data.tel3_pixel_ID);
      event_data.tels_in_event[2]	= 3;
      break;
    case 4 : pixel_intensity 				= &(event_data.tel4_image);
      pixel_ID 						= &(event_data.tel4_pixel_ID);
      event_data.tels_in_event[3] 	= 4;
      break;
    case 5 : pixel_intensity 				= &(event_data.tel5_image);
      pixel_ID 						= &(event_data.tel5_pixel_ID);
      event_data.tels_in_event[4] 	= 5;
      break;
    }// end switch

    const Sash::IntensityData* intensityData = (*tel_it)->Get<Sash::IntensityData>("intensity");
    const Sash::PointerSet<Sash::Pixel>& imagelist = intensityData->GetDataMode() == Sash::IntensityData::ZeroSup ? 
      intensityData->GetNonZero() : (*tel_it)->GetConfig()->GetPixelsInTel();

    for( Sash::PointerSet<Sash::Pixel>::const_iterator pix_it = imagelist.begin(); pix_it != imagelist.end(); ++pix_it ) {
      pixel_intensity->push_back( intensityData->GetIntensity(*pix_it)->fIntensity );
      //std::string pxid = (*tel_it)->GetPixelFast(*pix_it).GetIdentifier();
      int pxid = (*pix_it)->GetConfig()->GetPixelID();
      //std::cout << "DEBUG pxid=" << pxid << std::endl;
      pixel_ID->push_back(pxid);
      //std::cout << "DEBUG JPL: pixel_ID=" << pixel_ID->back() << ", intens="  << pixel_intensity->back() << std::endl;
    }//end pixels loop
  }// end telescopes loop

  bool one 	= ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 1 ) );
  bool two 	= ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 2 ) );
  bool three 	= ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 3 ) );
  bool four 	= ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 4 ) );
  bool five 	= ( std::end( event_data.tels_in_event ) != std::find( std::begin( event_data.tels_in_event ), std::end( event_data.tels_in_event ), 5 ) );

  event_data.num_of_images = one + two + three + four + five;

  std::stringstream ss;
  ss << "[";
  for(size_t i = 0; i < 5; ++i){
    if(i != 0)
      ss << ",";
    ss << event_data.tels_in_event[i];
  }
  ss << "]";
  event_data.event_tels_str = ss.str();

  if( !one ){
    event_data.tel1_image		= {std::nan("1")};
    event_data.tel1_pixel_ID	= {-1};
  }

  if( !two ){
    event_data.tel2_image		= {std::nan("1")};
    event_data.tel2_pixel_ID	= {-1};
  }

  if( !three ){
    event_data.tel3_image		= {std::nan("1")};
    event_data.tel3_pixel_ID	= {-1};
  }

  if( !four ){
    event_data.tel4_image		= {std::nan("1")};
    event_data.tel4_pixel_ID	= {-1};
  }

  if( !five ){
    event_data.tel5_image		= {std::nan("1")};
    event_data.tel5_pixel_ID	= {-1};
  }
}// end write_MCIntensityDataevent_to_event_struct

// char const * get_rawdata_list(){
//   //return char * file_name = "run_14011001_1";
//   return "run_14011001_1.root";
// }// end get_rawdata_list

TFile* open_rawdata_file( char const * rawdatafile )
{
  TFile* f = new TFile( rawdatafile );
  if( !f->IsOpen() ) {
    std::cerr << "Error: could not open " << rawdatafile << std::endl;
    return 0;
  }
  else if( f->IsOpen() ){
    std::cout << "Opened MC Smash file: " << rawdatafile << std::endl;
  }
  return f;
}// end open_rawdata_file

void write_run_header_data( run_data_struct &run_data, Sash::HESSArray* hess, const Sash::RunHeader* runheader )
{
  run_data.run_number = (*runheader).GetRunNum();

  // =============================================================
  // tel info (count tels in run and make list):
  // =============================================================
  const Sash::PointerSet<Sash::Telescope> &telsinrun=runheader->GetTelsInRun();
  Sash::PointerSet<Sash::Telescope>::iterator tel = telsinrun.begin();
  int ntels = 0;
  std::vector<int> tels;
  std::ostringstream ost;
  std::map<int,int>  telIdToIndex; //! maps telescope id to index for array-based parameters
  int idx=0;
  for(; tel != telsinrun.end();++tel) {
    ntels++;
    int id = (*tel)->HandleConfig()->GetTelId();
    tels.push_back( id  );
    telIdToIndex[id] = idx;
    idx++;
  }

  std::stringstream ss;
  ss << "[";
  for(size_t i = 0; i < tels.size(); ++i){
    if(i != 0)
      ss << ",";
    ss << tels[i];
  }
  ss << "]";
  run_data.tels_str = ss.str();
  run_data.ntels = ntels;

  Stash::Coordinate obs = runheader->GetObservationPosition();
	
  Stash::Coordinate altaz = obs.GetCoordinate(*hess->GetAltAzSystem());
  run_data.obspos_alt = altaz.GetBeta().GetDegrees();
  run_data.obspos_az  = altaz.GetLambda().GetDegrees();
}

int main(int argc, char *argv[])
{
  if(argc!=2){
    std::cout << "Please provide an input ROOT file" << std::endl;
    return 1;
  }

  const std::string filename = argv[1];

  std::ostringstream outfilename;
  outfilename << filename << ".h5";

  //char const * list = get_rawdata_list();
  //char const * rawdatafile = list;
  char const * rawdatafile = filename.c_str();
  std::cout << rawdatafile << std::endl;
  TFile* f = open_rawdata_file( rawdatafile );

  Sash::HESSArray* hess = &Sash::HESSArray::GetHESSArray();
  if (hess == NULL){
    std::cout << "Could not initialise an HESSArray" << std::endl;
    return 0;
  }

  Sash::DataSet* sash_data = (Sash::DataSet*)f->Get("run");
  sash_data->GetEntry(0);

  const Sash::RunHeader* runheader = hess->Get<Sash::RunHeader>();

  Sash::DataSet* events = (Sash::DataSet*)f->Get("calibratedevents");
  if( !events ) {
    std::cerr << "Error: could not access events" << std::endl;
    return 0;
  }

  run_data_struct run_data;
  std::vector<event_data_struct> events_array;

  write_run_header_data( run_data, hess, runheader );

  std::cout << std::endl << "reading events using Sash/DataSetIterator..." << std::endl;
  int i = 0;
  for( Sash::DataSetIterator it = events->begin(); it != events->end(); ++i ){
    // if(i == 450000)
    //   break;
    it.Process(0,0);
    if(i%10000==0) {
      std :: cout << "DEBUG Processing event " << i << std::endl;
    //   int ndigits = numDigits(i);
    //   for(int k=0; k<ndigits; k++) std::cout << "\b";
    //   std::cout << i;
    }
    event_data_struct event;
    write_MCIntensityDataevent_to_event_struct( event, hess, i );
    events_array.push_back(event);

    events_array[i].tel1_image_Handle.len 		= events_array[i].tel1_image.size();
    events_array[i].tel1_image_Handle.p 		= events_array[i].tel1_image.data();
    events_array[i].tel2_image_Handle.len 		= events_array[i].tel2_image.size();
    events_array[i].tel2_image_Handle.p 		= events_array[i].tel2_image.data();
    events_array[i].tel3_image_Handle.len 		= events_array[i].tel3_image.size();
    events_array[i].tel3_image_Handle.p 		= events_array[i].tel3_image.data();
    events_array[i].tel4_image_Handle.len 		= events_array[i].tel4_image.size();
    events_array[i].tel4_image_Handle.p 		= events_array[i].tel4_image.data();
    events_array[i].tel5_image_Handle.len 		= events_array[i].tel5_image.size();
    events_array[i].tel5_image_Handle.p 		= events_array[i].tel5_image.data();

    events_array[i].tel1_pixel_ID_Handle.len 	= events_array[i].tel1_pixel_ID.size();
    events_array[i].tel1_pixel_ID_Handle.p 		= events_array[i].tel1_pixel_ID.data();
    events_array[i].tel2_pixel_ID_Handle.len 	= events_array[i].tel2_pixel_ID.size();
    events_array[i].tel2_pixel_ID_Handle.p 		= events_array[i].tel2_pixel_ID.data();
    events_array[i].tel3_pixel_ID_Handle.len 	= events_array[i].tel3_pixel_ID.size();
    events_array[i].tel3_pixel_ID_Handle.p 		= events_array[i].tel3_pixel_ID.data();
    events_array[i].tel4_pixel_ID_Handle.len 	= events_array[i].tel4_pixel_ID.size();
    events_array[i].tel4_pixel_ID_Handle.p 		= events_array[i].tel4_pixel_ID.data();
    events_array[i].tel5_pixel_ID_Handle.len 	= events_array[i].tel5_pixel_ID.size();
    events_array[i].tel5_pixel_ID_Handle.p 		= events_array[i].tel5_pixel_ID.data();
  }// end for loop
  std::cout << std::endl;

  std::cout << i << " events read." << std::endl;
  hsize_t nevents = events_array.size();
  std::cout << "\n\nN = " << nevents << std::endl;

  hsize_t dim_tel[] = { 5 };

  hsize_t dim_evt[] = { nevents };	
  int rank_evt = sizeof(dim_evt) / sizeof(hsize_t);

  hsize_t dim_run[] = { 1 };
  int rank_run = sizeof(dim_run) / sizeof(hsize_t);

  hid_t vlenF_tid = H5Tvlen_create(H5T_NATIVE_FLOAT);
  hid_t vlenC_tid = H5Tvlen_create(H5T_NATIVE_INT);
  // hid_t vlenC_tid = H5Tvlen_create(H5T_C_S1); // H5T_NATIVE_CHAR
  //hid_t vlenC_tid = H5Tcopy(H5T_C_S1); // H5T_NATIVE_CHAR
  //herr_t tmpstatus = H5Tset_size(vlenC_tid, H5T_VARIABLE);
  hid_t array_tid = H5Tarray_create( H5T_NATIVE_INT, 1, dim_tel );

  hid_t event_tid = H5Tcreate( H5T_COMPOUND, sizeof( event_data_struct ) );
  H5Tinsert( event_tid, "Event_ID", HOFFSET(event_data_struct, eventID), H5T_NATIVE_LLONG );
  H5Tinsert( event_tid, "Tels_in_event", HOFFSET(event_data_struct, tels_in_event), array_tid );
  H5Tinsert( event_tid, "Num_of_images", HOFFSET(event_data_struct, num_of_images), H5T_NATIVE_INT );

  H5Tinsert( event_tid, "CT1_image", HOFFSET(event_data_struct, tel1_image_Handle), vlenF_tid );
  H5Tinsert( event_tid, "CT2_image", HOFFSET(event_data_struct, tel2_image_Handle), vlenF_tid );
  H5Tinsert( event_tid, "CT3_image", HOFFSET(event_data_struct, tel3_image_Handle), vlenF_tid );
  H5Tinsert( event_tid, "CT4_image", HOFFSET(event_data_struct, tel4_image_Handle), vlenF_tid );
  H5Tinsert( event_tid, "CT5_image", HOFFSET(event_data_struct, tel5_image_Handle), vlenF_tid );

  H5Tinsert( event_tid, "CT1_pixel_ID", HOFFSET(event_data_struct, tel1_pixel_ID_Handle), vlenC_tid );
  H5Tinsert( event_tid, "CT2_pixel_ID", HOFFSET(event_data_struct, tel2_pixel_ID_Handle), vlenC_tid );
  H5Tinsert( event_tid, "CT3_pixel_ID", HOFFSET(event_data_struct, tel3_pixel_ID_Handle), vlenC_tid );
  H5Tinsert( event_tid, "CT4_pixel_ID", HOFFSET(event_data_struct, tel4_pixel_ID_Handle), vlenC_tid );
  H5Tinsert( event_tid, "CT5_pixel_ID", HOFFSET(event_data_struct, tel5_pixel_ID_Handle), vlenC_tid );

  H5Tinsert( event_tid, "MonteCarloID", HOFFSET(event_data_struct, mcID), H5T_NATIVE_LLONG );
  H5Tinsert( event_tid, "MC_showerID", HOFFSET(event_data_struct, showerID), H5T_NATIVE_LLONG );
  H5Tinsert( event_tid, "PrimaryID", HOFFSET(event_data_struct, primaryID), H5T_NATIVE_INT );
  H5Tinsert( event_tid, "Energy", HOFFSET(event_data_struct, energy), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "Altitude", HOFFSET(event_data_struct, alt), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "Azimuth", HOFFSET(event_data_struct, az), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "CoreX", HOFFSET(event_data_struct, coreX), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "CoreY", HOFFSET(event_data_struct, coreY), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "CoreZ", HOFFSET(event_data_struct, coreZ), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "XStart", HOFFSET(event_data_struct, xStart), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "XMax", HOFFSET(event_data_struct, xMax), H5T_NATIVE_FLOAT );
  H5Tinsert( event_tid, "First_int_alt", HOFFSET(event_data_struct, firstInteractionAlt), H5T_NATIVE_FLOAT );


  hid_t run_tid = H5Tcreate( H5T_COMPOUND, sizeof( run_data_struct ) );
  H5Tinsert( run_tid, "Run_ID", HOFFSET(run_data_struct, run_number), H5T_NATIVE_UINT );
  H5Tinsert( run_tid, "NTels", HOFFSET(run_data_struct, ntels), H5T_NATIVE_INT );
  H5Tinsert( run_tid, "ObsAlt", HOFFSET(run_data_struct, obspos_alt), H5T_NATIVE_FLOAT );
  H5Tinsert( run_tid, "ObsAz", HOFFSET(run_data_struct, obspos_az), H5T_NATIVE_FLOAT );

  std::cout << "Writing MCIntensityData in HDF5 format..." << std::endl;

  hid_t event_space 	= H5Screate_simple(rank_evt, dim_evt, NULL);
  hid_t run_space 	= H5Screate_simple(rank_run, dim_run, NULL);

  hid_t file = H5Fcreate(outfilename.str().c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  hid_t event_dataset = H5Dcreate(file, "Events_List", event_tid, event_space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Dwrite(event_dataset, event_tid, H5S_ALL, H5S_ALL, H5P_DEFAULT, &events_array[0]);

  hid_t run_dataset = H5Dcreate(file, "Run_Data", run_tid, run_space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Dwrite( run_dataset, run_tid, H5S_ALL, H5S_ALL, H5P_DEFAULT, &run_data );

  std::cout << "HDF5 file written!" << std::endl;

  H5Dclose( event_dataset );
  H5Dclose( run_dataset );
  H5Sclose( event_space );
  H5Sclose( run_space );
  H5Tclose( event_tid );
  H5Tclose( run_tid );
  H5Fclose( file );

  /*
  //DEBUG
  std::cout << 0 << std::endl;
  std::cout << "event_num = " << events_array[0].eventID << std::endl;
  std::cout << "MC shower Id = " << events_array[0].showerID << std::endl;
  std::cout << "primary ID = " << events_array[0].primaryID << std::endl;
  std::cout << "energy = " << events_array[0].energy << std::endl;

  std::cout << "MC alt = " << events_array[0].alt << std::endl;

  std::cout << "tels in event are " << events_array[0].event_tels_str << std::endl;
  std::cout << "num of images is " << events_array[0].num_of_images << std::endl;

  std::cout << "num of pixels = " << events_array[0].tel1_image.size() << std::endl;

  for (std::vector<float>::const_iterator i = events_array[0].tel2_image.begin(); i != events_array[0].tel2_image.end(); ++i){
  std::cout << *i << ' ';
  }
  std::cout << std::endl;
  for (std::vector<int>::const_iterator i = events_array[0].tel2_pixel_ID.begin(); i != events_array[0].tel2_pixel_ID.end(); ++i)
  std::cout << *i << ' ';
  std::cout << std::endl;
  std::cout << std::endl;

  for (std::vector<float>::const_iterator i = events_array[0].tel3_image.begin(); i != events_array[0].tel3_image.end(); ++i){
  std::cout << *i << ' ';
  }
  std::cout << std::endl;
  for (std::vector<int>::const_iterator i = events_array[0].tel3_pixel_ID.begin(); i != events_array[0].tel3_pixel_ID.end(); ++i)
  std::cout << *i << ' ';
  std::cout << std::endl;
  std::cout << std::endl;*/

  return 0;
}
