import h5py
import numpy as np

dst = h5py.File('new.h5')

events_array = dst["Events_List"]
run_data = dst['Run_Data']

runID = run_data['Run_ID']
ntels = run_data["NTels"]

eventID = events_array['Event_ID']
showerId = events_array['MC_showerID']
primaryID = events_array['PrimaryID']

energy = events_array['Energy']
alt = events_array['Altitude']

tel1_width = events_array['CT1_width']
tel3_width = events_array['CT3_width']
tel1_kurtosis = events_array['CT1_kurtosis']
tel3_kurtosis = events_array['CT3_kurtosis']

image1 = events_array['CT1_image']
image1_pixels = events_array['CT1_pixel_ID']

image2 = events_array['CT2_image']
image2_pixels = events_array['CT2_pixel_ID']

image3 = events_array['CT3_image']
image3_pixels = events_array['CT3_pixel_ID']

image4 = events_array['CT4_image']
image4_pixels = events_array['CT4_pixel_ID']

image5 = events_array['CT5_image']
image5_pixels = events_array['CT5_pixel_ID']